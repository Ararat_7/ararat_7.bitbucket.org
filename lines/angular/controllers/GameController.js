app.controller('GameController', ['$scope', '$timeout','BoardFactory', 'BallFactory', '$http', function ($scope, $timeout, BoardFactory, BallFactory, $http) {
    var game = {
        score: 0,
        highScore: 0,
        balls: BoardFactory.ballsObj.balls,
        board: BoardFactory.getBoard(),

        cellClick: function (position) {
            if (BoardFactory.isCellEmpty(position)) {
                if (BoardFactory.moveBall(position)) {
                    var ballsToRemove = BoardFactory.checkLines();
                    if (ballsToRemove.length > 4) {
                        BoardFactory.removeBalls(ballsToRemove);
                        $scope.score += (ballsToRemove.length - 5) * 2 + 8;
                    } else {
                        BallFactory.createBall(3);
                        ballsToRemove = BoardFactory.checkLines();
                        if (ballsToRemove.length > 4) {
                            BoardFactory.removeBalls(ballsToRemove);
                            $scope.score += (ballsToRemove.length - 5) * 2 + 8;
                        }
                    }
                    $timeout(function () {
                        if (BoardFactory.getEmptyCells().length === 0) {
                            $scope.messageBox.show('Game over', 'Your score: ' + $scope.score + '.');
                            ($scope.score > $scope.highScore) && updateHighScore($scope.score);
                        }
                    });
                } else $scope.shadowAlert.show();
            }
        },

        newGame: function () {
            BoardFactory.resetBoard();
            $scope.balls = BoardFactory.ballsObj.balls;
            $scope.score = 0;
            BallFactory.createBall(3);
        },

        shadowAlert: {
            visible: false,
            show: function () {
                $scope.shadowAlert.visible = true;
            },
            hide: function () {
                $scope.shadowAlert.visible = false;
            }
        },

        messageBox: {
            visible: false,
            title: '',
            message: '',

            show: function (title, message) {
                $scope.messageBox.title = title || '';
                $scope.messageBox.message = message || '';
                $scope.messageBox.visible = true;
            },
            hide: function () {
                $scope.messageBox.visible = false;
                $scope.messageBox.title = '';
                $scope.messageBox.message = '';
            }
        }
    };

    angular.extend($scope, game);

    function getHighScore() {
        //$http.get('/high-score').then(function (response) {
        //    if (response.data.status === 'ok' && response.data.high_score) {
        //        $scope.highScore = response.data.high_score;
        //    }
        //})
    }

    function updateHighScore(score) {
        //$http.post('/high-score', {score: score}).then(function (response) {
        //    if (response.data.status === 'ok' && response.data.new_high_score) {
        //        $scope.highScore = score;
        //    }
        //}, function (response) {
        //    console.log('--- error ---');
        //    console.log(response);
        //});
    }

    (function init() {
        getHighScore();
        BallFactory.createBall(3);
    })();
}]);