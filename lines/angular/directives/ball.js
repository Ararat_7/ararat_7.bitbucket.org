app.directive('ball', ['$document', 'BoardFactory', function ($document, BoardFactory) {
    var cellSize = 50;
    var ballSize = 34;

    function setBall(ball, position) {
        var top = position.y * cellSize + (cellSize - ballSize)/2;
        var left = position.x * cellSize + (cellSize - ballSize)/2;
        ball.css({
            'height': ballSize + 'px',
            'width': ballSize + 'px',
            'position': 'absolute',
            'top': top + 'px',
            'left': left + 'px'
        });
    }

    function link(scope, elem, attrs) {

        var $ball = elem.find('.ball');

        setBall($ball, scope.params.position);

        var colors = scope.params.colors;
        var bgr = 'radial-gradient(circle at 50% 120%, ' + colors[3] + ', ' + colors[2] + ' 10%, ' + colors[1] + ' 80%, ' + colors[0] + ' 100%)';
        $ball.css('background', bgr);

        $ball.on('click', function (e) {
            BoardFactory.selectedBallObj.selected = scope.params;
            $ball.closest('.board').find('.ball').removeClass('selected');
            $ball.addClass('selected');
        });

        scope.$watch('params', function(newVal){
            setBall($ball, newVal.position);
            $ball.closest('.board').find('.ball').removeClass('selected');
        }, true);
    }

    return {
        scope: {
            params: '='
        },
        restrict: 'E',
        template: '<div class="ball" ng-class="{\'selected\' : isSelected}"></div>',
        link: link
    }
}]);