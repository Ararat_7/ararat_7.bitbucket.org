app.directive('shadowAlert', ['$document', '$timeout', function ($document, $timeout) {
    function link(scope, elem, attrs) {
        scope.$watch('shadowAlert', function (newVal) {
            if (newVal) {
                elem.addClass('alert');
                $timeout(function () {
                    scope.shadowAlert = false;
                }, 380);
            } else {
                elem.removeClass('alert');
            }
        });
    }

    return {
        scope: {
            shadowAlert: '='
        },
        restrict: 'A',
        link: link
    }
}]);