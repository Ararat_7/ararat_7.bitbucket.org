app.factory('BallFactory', ['BoardFactory', function (BoardFactory) {
    var colors = {
        red: ['#B71C1C', '#C62828', '#D32F2F', '#E53935'],
        blue: ['#0D47A1', '#1565C0', '#1976D2', '#1E88E5'],
        green: ['#1B5E20', '#2E7D32', '#388E3C', '#43A047'],
        yellow: ['#F57F17', '#F9A825', '#FBC02D', '#FDD835'],
        brown: ['#3E2723', '#4E342E', '#5D4037', '#6D4C41'],
        pink: ['#880E4F', '#AD1457', '#C2185B', '#D81B60'],
        grey: ['#212121', '#424242', '#616161', '#757575']
    };

    var generateRandomPosition = function () {
        var emptyCells = BoardFactory.getEmptyCells();
        if (emptyCells.length === 0) return false;

        var random = Helper.getRandomIntInclusive(0, emptyCells.length - 1);
        var cell = emptyCells[random];

        return cell.position;
    };

    function createBall(count) {
        var ball = {};
        ball.id = Helper.uuid();

        var keys = Object.keys(colors);

        var randomInt = Helper.getRandomIntInclusive(0, keys.length - 1);

        ball.colors = colors[keys[randomInt]];
        ball.colorName = keys[randomInt];

        var position = generateRandomPosition();
        if (position) {
            ball.position = position;
        } else {
            return false;
        }

        BoardFactory.addBall(ball);

        if (count && !isNaN(count)) {
            --count && createBall(count);
        }

        return true;
    }

    return {
        createBall: createBall
    }
}]);