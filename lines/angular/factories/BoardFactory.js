app.factory('BoardFactory', function () {
    var boardSize = 9;
    var board = [];
    var ballsObj = {
        balls: []
    };
    var selectedBallObj = {
        selected: null
    };

    function createBoard() {
        for (var i = 0; i < boardSize; i++) {
            var row = [];
            for (var j = 0; j < boardSize; j++) {
                row.push({
                    empty: true,
                    position: {
                        x: j,
                        y: i
                    }
                });
            }
            board.push(row);
        }
    }

    createBoard();

    function resetBoard() {
        board = [];
        ballsObj.balls = [];
        selectedBallObj.selected = null;
        createBoard();
    }

    function getEmptyCells() {
        var cellsArray = [];
        for (var i = 0; i < board.length; i++) {
            for (var j = 0; j < board[i].length; j++) {
                if (board[i][j].empty) {
                    cellsArray.push(board[i][j]);
                }
            }
        }

        return cellsArray;
    }

    function clearCell(position) {
        board[position.y][position.x] = {
            empty: true,
            position: {
                x: position.x,
                y: position.y
            }
        };
    }

    function removeBalls(balls) {
        for (var i = 0; i < balls.length; i++) {
            clearCell(balls[i].position);
            removeBallByPosition(balls[i].position);
        }
    }

    function removeBallByPosition(position) {
        for (var i = 0; i < ballsObj.balls.length; i++) {
            if (ballsObj.balls[i].position.x === position.x && ballsObj.balls[i].position.y === position.y) {
                ballsObj.balls.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    function getCell(position) {
        if (position.x < 0 || position.x > boardSize - 1 || position.y < 0 || position.y > boardSize - 1) {
            throw new Error('invalid params in getCell()!');
        }

        return board[position.y][position.x];
    }

    function getBoard() {
        return board;
    }

    function addBall(ball) {
        var position = ball.position;
        if (position.x < 0 || position.x > boardSize - 1 || position.y < 0 || position.y > boardSize - 1) {
            throw new Error('invalid params in addBall()!');
        }

        if (getCell(position).empty) {
            var cell = board[position.y][position.x];
            cell.empty = false;
            cell.ballColor = ball.colorName;

            ballsObj.balls.push(ball);
        }
    }

    function moveBall(endPos) {
        if (!selectedBallObj.selected) return false;
        if (!checkAvailability(selectedBallObj.selected.position, endPos)) {
            clearMarkedCells();
            return false;
        }
        clearMarkedCells();

        var startPos = selectedBallObj.selected.position;
        var index = Helper.findByProp(ballsObj.balls, 'id', selectedBallObj.selected.id);
        ballsObj.balls[index].position = endPos;    // move ball
        swapCells(startPos, endPos);                // correct board values
        selectedBallObj.selected = null;
        return true;
    }

    function checkAvailability(pos1, pos2) {
        if (!board[pos2.y][pos2.x].empty) return false;

        var freeNeighbours = getFreeNeighbours(pos1);

        if (freeNeighbours.length == 0) return false;

        if (existsInNeighbours(freeNeighbours, pos2)) return true;
        else {
            var result = false;
            for (var i = 0; i < freeNeighbours.length; i++) {
                result = checkAvailability(freeNeighbours[i].position, pos2);
                if (result) break;
            }
            return result;
        }
    }

    function getFreeNeighbours(pos) {
        var freeCells = [];
        var neighbours = [
            {x: +pos.x - 1, y: +pos.y},
            {x: +pos.x + 1, y: +pos.y},
            {x: +pos.x, y: +pos.y - 1},
            {x: +pos.x, y: +pos.y + 1}
        ];
        for (var i = 0; i < neighbours.length; i++) {
            if (neighbours[i].x >= 0 && neighbours[i].y >= 0 && neighbours[i].x < boardSize && neighbours[i].y < boardSize) {
                var cell = board[neighbours[i].y][neighbours[i].x];
                if (cell.empty && !cell.marked) {
                    cell.marked = true;
                    freeCells.push(cell);
                }
            }
        }

        return freeCells;
    }

    function clearMarkedCells() {
        for (var i = 0; i < boardSize; i++) {
            for (var j = 0; j < boardSize; j++) {
                delete board[i][j].marked;
            }
        }
    }

    function existsInNeighbours(arr, pos) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].position.x == pos.x && arr[i].position.y == pos.y) {
                return true;
            }
        }
        return false;
    }

    function swapCells(pos1, pos2) {
        var tempCell = board[pos1.y][pos1.x];
        board[pos1.y][pos1.x] = board[pos2.y][pos2.x];
        board[pos2.y][pos2.x] = tempCell;
        board[pos1.y][pos1.x]['position'] = pos1;   // fix positions
        board[pos2.y][pos2.x]['position'] = pos2;   // fix positions
        return true;
    }

    function isCellEmpty(pos) {
        return board[pos.y][pos.x].empty;
    }

    function checkLines() {
        return checkRows().concat(checkColumns().concat(checkDiagonals()));
    }

    function checkRows() {
        var checkedCells = [],
            currentCells = [],
            currentColor = '';
        for (var i = 0; i < boardSize; i++) {
            for (var j = 0; j < boardSize; j++) {
                if (board[i][j].empty || currentColor !== board[i][j].ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (board[i][j].ballColor) {
                        currentColor = board[i][j].ballColor;
                        currentCells.push(board[i][j]);
                    }
                } else {
                    currentCells.push(board[i][j]);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';
        }

        return checkedCells;
    }

    function checkColumns() {
        var checkedCells = [],
            currentCells = [],
            currentColor = '';
        for (var i = 0; i < boardSize; i++) {
            for (var j = 0; j < boardSize; j++) {
                if (board[j][i].empty || currentColor !== board[j][i].ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (board[j][i].ballColor) {
                        currentColor = board[j][i].ballColor;
                        currentCells.push(board[j][i]);
                    }
                } else {
                    currentCells.push(board[j][i]);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';
        }

        return checkedCells;
    }

    function checkDiagonals() {
        var checkedCells = [],
            currentCells = [],
            currentCell,
            currentColor = '';

        var d = 0;
        while (d < boardSize - 4) {
            for (var i = 0; i < boardSize - d; i++) {
                currentCell = board[i][i + d];
                if (currentCell.empty || currentColor !== currentCell.ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (currentCell.ballColor) {
                        currentColor = currentCell.ballColor;
                        currentCells.push(currentCell);
                    }
                } else {
                    currentCells.push(currentCell);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';

            for (i = 0; i < boardSize - d; i++) {
                if (d === 0) break;
                currentCell = board[i + d][i];
                if (currentCell.empty || currentColor !== currentCell.ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (currentCell.ballColor) {
                        currentColor = currentCell.ballColor;
                        currentCells.push(currentCell);
                    }
                } else {
                    currentCells.push(currentCell);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';

            for (i = 0; i < boardSize - d; i++) {
                currentCell = board[boardSize - 1 - i][i + d];
                if (currentCell.empty || currentColor !== currentCell.ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (currentCell.ballColor) {
                        currentColor = currentCell.ballColor;
                        currentCells.push(currentCell);
                    }
                } else {
                    currentCells.push(currentCell);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';

            for (i = 0; i < boardSize - d; i++) {
                if (d === 0) break;
                currentCell = board[boardSize - 1 - i - d][i];
                if (currentCell.empty || currentColor !== currentCell.ballColor) {
                    if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
                    currentCells = [];
                    currentColor = '';
                    if (currentCell.ballColor) {
                        currentColor = currentCell.ballColor;
                        currentCells.push(currentCell);
                    }
                } else {
                    currentCells.push(currentCell);
                }
            }
            if (currentCells.length > 4) checkedCells = checkedCells.concat(currentCells);
            currentCells = [];
            currentColor = '';

            d++;
        }

        return checkedCells;
    }

    return {
        getBoardSize: boardSize,
        getBoard: getBoard,
        getCell: getCell,
        getEmptyCells: getEmptyCells,
        addBall: addBall,
        ballsObj: ballsObj,
        selectedBallObj: selectedBallObj,
        moveBall: moveBall,
        isCellEmpty: isCellEmpty,
        checkLines: checkLines,
        removeBalls: removeBalls,
        resetBoard: resetBoard
    };
});