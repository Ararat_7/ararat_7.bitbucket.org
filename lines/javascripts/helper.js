var Helper = (function (){
    function isArray(obj) {
        return (Object.prototype.toString.call(obj) === '[object Array]');
    }

    function getRandomIntInclusive(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    function findByProp(arr, prop, val) {
        if (!isArray(arr) || prop === null || prop === undefined || val === null || val === undefined)
            throw new Error('invalid arguments in findByProp()!');

        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === val) return i;
        }

        return -1;
    }

    return {
        getRandomIntInclusive: getRandomIntInclusive,
        uuid: uuid,
        isArray: isArray,
        findByProp: findByProp
    }
})();